package main

import (
	"errors"
	"fmt"
	"math"
)

const bottomFloor = 1
const topFloor = 5
const numElevators = 2
const maxTrips = 100

// List of elevators
var elevators []elevator

type request struct {
	origin, dest int
}

// Hold requests (allows retrying when no elevator available
var requestQueue []request

type elevator struct {
	name         int
	moving       bool
	open         bool
	floor        int
	destFloors   []int
	trips        int
	floorsPassed int
	maintenance  bool
}

func main() {
	requestQueue := make([]request, 0)
	elevators = make([]elevator, numElevators, numElevators)

	// Populate elevators with defaults
	for i := range elevators {
		e := &elevators[i]
		e.name = i
		e.moving = false
		e.open = false
		e.floor = bottomFloor
		e.destFloors = make([]int, 0)
		e.trips = 0
		e.floorsPassed = 0
		e.maintenance = false
	}

	fmt.Println(elevators, requestQueue)

	// start
	for {
		// Get next request and process
		r, e := getNextRequest()
		if e != nil {
			requestFloor(r.origin, r.dest)
		}

		// Loop through elevators
		for i := range elevators {
			e := &elevators[i]

			// If in maintenance mode, skip
			if e.maintenance {
				continue
			}

			// Check destination
			e.checkDestination()

			// Move
			e.move()
		}
	}
}

func requestFloor(origin, dest int) {
	if dest < bottomFloor || dest > topFloor || origin < bottomFloor || origin > topFloor {
		return
	}

	e, error := getAvailableElevator(origin)
	if error != nil {
		fmt.Println(error)
		// re-queue the request
		requestQueue = append(requestQueue, request{origin, dest})
	} else {
		e.unshiftDest(dest)
		e.unshiftDest(origin)
	}
}

func getAvailableElevator(origin int) (*elevator, error) {
	closest := -1

	for i, e := range elevators {
		if !e.moving && e.floor == origin { // if available and on the origin floor just return it
			return &e, nil
		}

		if e.moving { // If moving
			// if below it and moving above it or to it
			if e.floor < origin && e.destFloors[0] >= origin {
				closest = i
				continue
			}

			// if above it and moving below it or to it
			if e.floor > origin && e.destFloors[0] <= origin {
				closest = i
				continue
			}
		}

		if !e.moving { // if not moving
			if closest == -1 { // if first one evaluated here
				closest = i
			} else if math.Abs(float64(e.floor-origin)) < math.Abs(float64(elevators[closest].floor-origin)) {
				// if closer than last closest one
				closest = i
			}
		}

	}

	// if no available elevators
	if closest == -1 {
		return nil, errors.New("No available elevator")
	}

	return &elevators[closest], nil
}

func (e *elevator) unshiftDest(floor int) {
	// prepend destination
	e.destFloors = append([]int{floor}, e.destFloors...)
	e.moving = true
}

func (e *elevator) checkDestination() {
	if !e.moving {
		return
	}
	fmt.Printf("check destination %d %d\n", e.floor, e.destFloors[0])
	if e.destFloors[0] == e.floor {
		if !e.open {
			e.open = true
			fmt.Printf("%d opening doors\n", e.name)
		}

		e.destFloors = remove(e.destFloors, 0)

		// Stop moving if no more destinations
		if len(e.destFloors) == 0 {
			e.moving = false
			e.trips++

			if e.trips > maxTrips {
				e.maintenance = true
			}
		}
	}
}

func (e *elevator) move() {
	if e.moving {
		// close doors if needed
		if e.open {
			e.open = false
			fmt.Printf("%d closing doors\n", e.name)
		}

		// move elevator one floor
		if e.floor < e.destFloors[0] {
			e.floor++
		} else {
			e.floor--
		}
		e.floorsPassed++
		fmt.Printf("elevator %d moved to floor %d\n", e.name, e.floor)
	}
}

func remove(s []int, i int) []int {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

// Get next request or return error if not requests in queue
func getNextRequest() (request, error) {
	// Get next request from queue
	return request{1, 1}, nil
}
